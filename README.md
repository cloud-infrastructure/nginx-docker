dockerfiles-centos-nginx
========================

CentOS 7 Dockerfile for nginx, This Dockerfile uses https://www.softwarecollections.org/en/scls/rhscl/rh-nginx18/

This build of nginx listens on port 80 by default. Please be aware of this
when you launch the container.

error_log and access_log will go to STDOUT.

You need to provide the configuration of the nginx server listen.
Please mount a volume on /etc/nginx/conf.d/ and place there the config files for your application
