FROM centos:centos7
MAINTAINER Jose Castro Leon <jose.castro.leon@cern.ch>

# Install epel-release & then the rest
RUN yum -y install epel-release && \
    yum -y install nginx

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/nginx.conf

VOLUME "/etc/nginx/conf.d"

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
